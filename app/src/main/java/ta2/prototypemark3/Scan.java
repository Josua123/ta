package ta2.prototypemark3;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

public class Scan extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case R.id.option1 : {
                Intent nextScreen = new Intent(getApplicationContext(), Home.class);
                startActivity(nextScreen);
                break;
            }
            case R.id.option2 : {
                Intent nextScreen = new Intent(getApplicationContext(), Scan.class);
                startActivity(nextScreen);
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

}
